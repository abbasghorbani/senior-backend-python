from fastapi import Header, HTTPException, Request
from src.models.permission import CategoryPermission, DocumentPermission
from src.models.member import Member
from src.models.document import Document


async def has_perm(request: Request = Request, username: str = Header(default=None)):
    status = False

    if username == "admin":
        status = True
        return status
    else:
        perm = ""
        method_type = request.method
        try:
            body = await request.json()
        except Exception as err:
            print(err)
            body = dict()

        document_id = request.path_params.get("document_id")
        category_id = request.path_params.get("category_id")
        print("mehod type", method_type)
        if method_type == "GET":
            perm = "read"
        elif method_type == "POST":
            perm = "create"
        elif method_type == "PUT":
            perm = "update"
        elif method_type == "DELETE":
            perm = "delete"
        else:
            raise HTTPException(status_code=405, detail="Method not alloweddddd")

        status = await check_permission(category_id, username, document_id, perm, body)
        if not status:
            raise HTTPException(status_code=403, detail="You do not have access to this content")
    return status


async def check_permission(category_id, username, document_id, perm, body):
    status = False
    cat_id = category_id if category_id else body.get("category_id")
    doc_id = document_id if document_id else body.get("document_id")

    member = await Member.get_or_none(username=username)
    if member is None:
        raise HTTPException(status_code=401, detail="Unauthorized username")
    member_id = member.id

    if cat_id:
        has_perm = await CategoryPermission.has_perm(member_id, cat_id, perm)
        print(has_perm, perm)
        if has_perm:
            status = True

    if doc_id:
        doc = await Document.get(id=int(doc_id))
        has_perm = CategoryPermission.has_perm(member_id, doc.category_id, perm)
        if has_perm:
            status = True
        else:
            has_perm = await DocumentPermission.has_perm(member_id, doc_id, perm)
            if has_perm:
                status = True
    return status
