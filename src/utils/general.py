def get_permission_field(name):
    result = ""

    if name == "read":
        result = "can_read"
    elif name == "create":
        result = "can_create"
    elif name == "update":
        result = "can_update"
    else:
        result = "can_delete"

    return result
