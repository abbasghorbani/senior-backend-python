from fastapi import Header, HTTPException
from src.models.member import Member


async def get_current_user(username: str = Header(default=None)) -> Member:
    user = await Member.get_or_none(username=username)
    if user is None:
        raise HTTPException(status_code=401, detail="Unauthorized username")
    return user
