from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise

from src import settings


def create_database_url() -> str:
    database_url = ""

    if settings.environment == "production":
        database_url = "{}://{}:{}@{}:{}/{}".format(
            settings.db, settings.db_user, settings.db_password, settings.db_host, settings.db_port, settings.db_name
        )
    elif settings.environment == "develop":
        database_url = "sqlite://db.sqlite3"
    else:
        database_url = "sqlite://:memory:"

    return database_url


def init_db(app: FastAPI) -> None:
    register_tortoise(
        app,
        db_url=create_database_url(),
        modules={"models": ["src.models.member", "src.models.document", "src.models.permission"]},
        generate_schemas=True,
        add_exception_handlers=True,
    )
