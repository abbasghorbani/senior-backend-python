from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from src.configs.database import init_db
from src.routers import category, document, member, permission

origins = [
    "http://localhost:9092",
]

app = FastAPI()


app.include_router(member.router, prefix="/member")
app.include_router(category.router, prefix="/category")
app.include_router(document.router, prefix="/document")
app.include_router(permission.router, prefix="/permission")

# Start DB Connection on Startup
@app.on_event("startup")
async def startup_event():
    init_db(app)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
