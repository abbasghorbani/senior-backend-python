import pytest
from httpx import AsyncClient


@pytest.mark.anyio
async def test_create_member(client: AsyncClient):
    data = {"username": "test"}
    response = await client.post("/member/", json=data, headers={"username": "admin"})

    assert response.json() == dict(data, id=2)
    assert response.status_code == 200


@pytest.mark.anyio
async def test_get_members(client: AsyncClient):
    data = {"username": "test"}
    response = await client.post("/member/", json=data, headers={"username": "admin"})

    assert response.json() == dict(data, id=2)
    assert response.status_code == 200

    response = await client.get("/member/", headers={"username": "admin"})
    assert response.status_code == 200
    assert response.json() == [{"username": "admin", "id": 1}, dict(data, id=2)]


@pytest.mark.anyio
async def test_update_member(client: AsyncClient):
    data = {"username": "test"}
    response = await client.post("/member/", json=data, headers={"username": "admin"})

    assert response.json() == dict(data, id=2)
    assert response.status_code == 200

    data1 = {"username": "test1"}
    response = await client.put(f"/member/{response.json()['id']}", json=data1, headers={"username": "admin"})
    assert response.status_code == 200
    assert response.json() == dict(data1, id=2)
    assert response.json() != dict(data, id=2)


@pytest.mark.anyio
async def test_delete_member(client: AsyncClient):
    data = {"username": "test"}
    response = await client.post("/member/", json=data, headers={"username": "admin"})

    assert response.json() == dict(data, id=2)
    assert response.status_code == 200

    response = await client.delete(f"/member/{response.json()['id']}", headers={"username": "admin"})
    assert response.status_code == 200

    response = await client.get("/member/", headers={"username": "admin"})
    assert response.status_code == 200
    assert response.json() == [{"username": "admin", "id": 1}]


@pytest.mark.anyio
async def test_unauthorized_user(client: AsyncClient):
    data = {"username": "test"}
    response = await client.post("/member/", json=data, headers={"username": "admin_test"})

    assert response.json() == {"detail": "Unauthorized username"}
    assert response.status_code == 401


@pytest.mark.anyio
async def test_not_admin_user(client: AsyncClient):
    data = {"username": "test"}
    response = await client.post("/member/", json=data, headers={"username": "admin"})

    assert response.json() == dict(data, id=2)
    assert response.status_code == 200

    response = await client.post("/member/", json=data, headers={"username": "test"})
    assert response.json() == {"detail": "You do not have access to this content"}
    assert response.status_code == 403
