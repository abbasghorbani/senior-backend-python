import pytest
from httpx import AsyncClient


@pytest.mark.anyio
async def test_create_document(client: AsyncClient):

    cat_data = {"name": "Report"}
    cat_response = await client.post("/category/", json=cat_data, headers={"username": "admin"})
    category_id = cat_response.json()["id"]

    doc_data = {"category_id": category_id, "subject": "test", "content": "test"}
    response = await client.post("/document/", json=doc_data, headers={"username": "admin"})

    assert response.status_code == 200
