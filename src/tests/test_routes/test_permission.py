import pytest
from httpx import AsyncClient


@pytest.mark.anyio
async def test_create_permission(client: AsyncClient):

    user_data = {"username": "test"}
    member_response = await client.post("/member/", json=user_data, headers={"username": "admin"})

    cat_data = {"name": "Report"}
    cat_response = await client.post("/category/", json=cat_data, headers={"username": "admin"})
    category_id = cat_response.json()["id"]

    doc_data = {"category_id": category_id, "subject": "test", "content": "test"}
    doc_response = await client.post("/document/", json=doc_data, headers={"username": "admin"})

    perm_data = {
        "member_id": member_response.json()["id"],
        "can_read": True,
        "can_create": False,
        "can_update": False,
        "can_delete": False,
        "document_id": doc_response.json()["id"],
    }
    perm_response = await client.post("/permission/create", json=perm_data, headers={"username": "admin"})

    assert perm_response.status_code == 200
    assert perm_response.json() == perm_data


@pytest.mark.anyio
async def test_access_document(client: AsyncClient):
    # Create new member
    user_data = {"username": "test"}
    member_response = await client.post("/member/", json=user_data, headers={"username": "admin"})

    # Create new category
    cat_data = {"name": "Report"}
    cat_response = await client.post("/category/", json=cat_data, headers={"username": "admin"})
    category_id = cat_response.json()["id"]

    # Create new document
    doc_data = {"category_id": category_id, "subject": "test", "content": "test"}
    doc_response = await client.post("/document/", json=doc_data, headers={"username": "admin"})

    # Create new permission
    perm_data = {
        "member_id": member_response.json()["id"],
        "can_read": True,
        "can_create": False,
        "can_update": False,
        "can_delete": False,
        "document_id": doc_response.json()["id"],
    }

    await client.post("/permission/create", json=perm_data, headers={"username": "admin"})

    access_doc_response = await client.get("/document/", headers={"username": "test"})

    assert access_doc_response.status_code == 200
    assert access_doc_response.json() == [doc_response.json()]


@pytest.mark.anyio
async def test_not_access_document(client: AsyncClient):
    # Create new member
    user_data = {"username": "test"}
    member_response = await client.post("/member/", json=user_data, headers={"username": "admin"})

    # Create new category
    cat_data = {"name": "Report"}
    cat_response = await client.post("/category/", json=cat_data, headers={"username": "admin"})
    category_id = cat_response.json()["id"]

    # Create new document
    doc_data = {"category_id": category_id, "subject": "test", "content": "test"}
    doc_response = await client.post("/document/", json=doc_data, headers={"username": "admin"})

    # Create new permission
    perm_data = {
        "member_id": member_response.json()["id"],
        "can_read": False,
        "can_create": False,
        "can_update": False,
        "can_delete": True,
        "document_id": doc_response.json()["id"],
    }

    await client.post("/permission/create", json=perm_data, headers={"username": "admin"})

    access_doc_response = await client.get("/document/", headers={"username": "test"})

    assert access_doc_response.status_code == 200
    assert access_doc_response.json() == []
