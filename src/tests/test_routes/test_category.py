import pytest
from httpx import AsyncClient


@pytest.mark.anyio
async def test_create_category(client: AsyncClient):
    data = {"name": "Report"}
    response = await client.post("/category/", json=data, headers={"username": "admin"})

    assert response.json() == dict(data, id=1)
    assert response.status_code == 200


@pytest.mark.anyio
async def test_get_categories(client: AsyncClient):
    data = {"name": "Report"}
    response = await client.post("/category/", json=data, headers={"username": "admin"})

    assert response.json() == dict(data, id=1)
    assert response.status_code == 200

    response = await client.get("/category/", headers={"username": "admin"})
    assert response.status_code == 200
    assert response.json() == [dict(data, id=1)]


@pytest.mark.anyio
async def test_update_category(client: AsyncClient):
    data = {"name": "Report"}
    response = await client.post("/category/", json=data, headers={"username": "admin"})

    assert response.json() == dict(data, id=1)
    assert response.status_code == 200

    data1 = {"name": "Sport"}
    response = await client.put(f"/category/{response.json()['id']}", json=data1, headers={"username": "admin"})
    assert response.status_code == 200
    assert response.json() == dict(data1, id=1)
    assert response.json() != dict(data, id=1)


@pytest.mark.anyio
async def test_delete_member(client: AsyncClient):
    data = {"name": "Report"}
    response = await client.post("/category/", json=data, headers={"username": "admin"})

    assert response.json() == dict(data, id=1)
    assert response.status_code == 200

    response = await client.delete(f"/category/{response.json()['id']}", headers={"username": "admin"})
    assert response.status_code == 200

    response = await client.get("/category/", headers={"username": "admin"})
    assert response.status_code == 200
    assert response.json() == []
