from tortoise import Model, fields
from tortoise.contrib.pydantic import pydantic_model_creator


class Member(Model):
    id = fields.IntField(pk=True)
    username = fields.CharField(max_length=128)

    @classmethod
    async def get_user_by_username(cls, username: str):
        return await cls.get_or_none(username=username)

    class PydanticMeta:
        exclude = ("documents",)
