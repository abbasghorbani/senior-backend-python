from tortoise import Tortoise
from tortoise.contrib.pydantic import pydantic_model_creator

from src.models.document import Category, Document, DocumentIn
from src.models.member import Member
from src.models.permission import DocumentPermission, CategoryPermission

Tortoise.init_models(["src.models.member", "src.models.document", "src.models.permission"], "models")

# Member schema
Member_Pydantic = pydantic_model_creator(Member, name="Member")
MemberIn_Pydantic = pydantic_model_creator(Member, name="MemberIn", exclude_readonly=True)

# Category schema
Category_Pydantic = pydantic_model_creator(Category, name="Category")
CategoryIn_Pydantic = pydantic_model_creator(Category, name="CategoryIn", exclude_readonly=True)

# Document schema
Document_Pydantic = pydantic_model_creator(Document, name="Document")
DocumentIn_Pydantic = DocumentIn

# CategoryPermission schema
CategoryPermission_Pydantic = pydantic_model_creator(CategoryPermission, name="CategoryPermission")

# DocumentPermission schema
DocumentPermission_Pydantic = pydantic_model_creator(DocumentPermission, name="DocumentPermission")

# print(Document_Pydantic.schema_json(indent=4))
