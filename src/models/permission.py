from tortoise import Model, fields
from typing import List
from pydantic import BaseModel, Field, validator, root_validator
from src.models.member import Member
from src.utils.general import get_permission_field


class CategoryPermission(Model):
    member_id = fields.IntField()
    category_id = fields.IntField()
    can_read = fields.BooleanField(default=False)
    can_create = fields.BooleanField(default=False)
    can_update = fields.BooleanField(default=False)
    can_delete = fields.BooleanField(default=False)

    class Meta:
        unique_together = (("member_id", "category_id"),)

    @classmethod
    async def get_can_read_cat_ids(cls, member_id):
        cat_ids = await cls.filter(member_id=member_id, can_read=True).values_list("category_id", flat=True)
        return cat_ids

    @classmethod
    async def has_perm(cls, member_id, category_id, perm):
        field_name = get_permission_field(perm)
        data = {"member_id": member_id, "category_id": category_id, field_name: True}
        return await cls.exists(**data)


class DocumentPermission(Model):
    member_id = fields.IntField()
    document_id = fields.IntField()
    can_read = fields.BooleanField(default=False)
    can_create = fields.BooleanField(default=False)
    can_update = fields.BooleanField(default=False)
    can_delete = fields.BooleanField(default=False)

    class Meta:
        unique_together = (("member_id", "document_id"),)

    @classmethod
    async def get_can_read_doc_ids(cls, member_id):
        ids = await cls.filter(member_id=member_id, can_read=True).values_list("document_id", flat=True)
        return ids

    @classmethod
    async def has_perm(cls, member_id, document_id, perm):
        field_name = get_permission_field(perm)
        data = {"member_id": member_id, "document_id": document_id, field_name: True}
        return await cls.exists(**data)


class Permission(BaseModel):
    member_id: int = Field(gt=0)
    can_read: bool = Field(default=False)
    can_create: bool = Field(default=False)
    can_update: bool = Field(default=False)
    can_delete: bool = Field(default=False)
    document_id: int = Field(gt=0, default=None)
    category_id: int = Field(gt=0, default=None)

    @validator("member_id")
    def validate_member_id(cls, value):
        try:
            Member.get(id=value)
            return value
        except Exception as err:
            print(err)
            raise ValueError("Invalid member_id. field")
        return value

    @root_validator(pre=True)
    def check_category_or_document_omitted(cls, values):
        cat_id = values.get("category_id")
        doc_id = values.get("document_id")
        if (cat_id and doc_id) or (cat_id is None and doc_id is None):
            raise ValueError("category_id OR document_id field should be included.Only one field.")
        return values
