from tortoise import Model, fields
from src.models.permission import DocumentPermission, CategoryPermission
from tortoise.expressions import Q
from pydantic import BaseModel, Field
from fastapi import Header


class Category(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=128)

    class PydanticMeta:
        exclude = ("documents",)

    def __str__(self):
        return self.subject


class Document(Model):
    id = fields.IntField(pk=True)
    author = fields.ForeignKeyField("models.Member")
    category = fields.ForeignKeyField("models.Category")
    subject = fields.CharField(max_length=255)
    content = fields.TextField()
    can_read: bool

    def __str__(self):
        return self.subject

    @classmethod
    async def get_user_docs(cls, member):
        if member.username == "admin":
            return cls.all()
        else:
            ids = await DocumentPermission.get_can_read_doc_ids(member_id=member.id)
            cat_ids = await CategoryPermission.get_can_read_cat_ids(member_id=member.id)
            return cls.filter(Q(id__in=ids) | Q(author=member.id) | Q(category_id__in=cat_ids))

    def get_can_read(self, flag):
        self.can_read = flag
        return self

    # class PydanticMeta:
    #     computed = ("can_read",)


class DocumentIn(BaseModel):
    category_id: int = Field(gt=0)
    subject: str = Field(max_length=255)
    content: str
