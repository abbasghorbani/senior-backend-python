from typing import List

from fastapi import APIRouter, Depends

from src.models.schema import Category, Category_Pydantic, CategoryIn_Pydantic
from src.utils.check_permission import has_perm

tags = ["Category"]
router = APIRouter(tags=tags)


@router.get("/", response_model=List[Category_Pydantic], dependencies=[Depends(has_perm)])
async def get_categories():
    return await Category_Pydantic.from_queryset(Category().all())


@router.post("/", response_model=Category_Pydantic, dependencies=[Depends(has_perm)])
async def create_category(form_data: CategoryIn_Pydantic):
    category_obj = await Category.create(**form_data.dict(exclude_unset=True))
    return await Category_Pydantic.from_tortoise_orm(category_obj)


@router.put("/{category_id}", response_model=Category_Pydantic, dependencies=[Depends(has_perm)])
async def update_user(category_id: int, category: CategoryIn_Pydantic):
    await Category.get(id=category_id).update(**category.dict(exclude_unset=True))
    return await Category_Pydantic.from_queryset_single(Category.get(id=category_id))


@router.delete("/{category_id}", dependencies=[Depends(has_perm)])
async def delete_user(category_id: int):
    await Category.filter(id=category_id).delete()
    return {}
