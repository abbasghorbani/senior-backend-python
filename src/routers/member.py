from typing import List

from fastapi import APIRouter, Depends

from src.models.schema import Member, Member_Pydantic, MemberIn_Pydantic
from src.utils.check_permission import has_perm

tags = ["Member"]
router = APIRouter(tags=tags)


@router.get("/", response_model=List[Member_Pydantic], dependencies=[Depends(has_perm)])
async def get_members():
    return await Member_Pydantic.from_queryset(Member.all())


@router.post("/", response_model=Member_Pydantic, dependencies=[Depends(has_perm)])
async def create_member(form_data: MemberIn_Pydantic):
    member_obj = await Member.create(**form_data.dict(exclude_unset=True))
    return await Member_Pydantic.from_tortoise_orm(member_obj)


@router.put("/{member_id}", response_model=Member_Pydantic, dependencies=[Depends(has_perm)])
async def update_user(member_id: int, member: MemberIn_Pydantic):
    await Member.get(id=member_id).update(**member.dict(exclude_unset=True))
    return await Member_Pydantic.from_queryset_single(Member.get(id=member_id))


@router.delete("/{member_id}", dependencies=[Depends(has_perm)])
async def delete_user(member_id: int):
    await Member.filter(id=member_id).delete()
    return {}
