from typing import List

from fastapi import APIRouter, Depends, HTTPException, Body

from src.models.member import Member
from src.models.schema import Document_Pydantic, DocumentIn_Pydantic
from src.models.document import Document
from src.models.permission import CategoryPermission
from src.utils.check_permission import has_perm
from src.utils.current_user import get_current_user

tags = ["Document"]
router = APIRouter(tags=tags)


@router.get("/", response_model=List[Document_Pydantic])
async def get_documents(user: Member = Depends(get_current_user)):
    docs = await Document.get_user_docs(member=user)
    return await Document_Pydantic.from_queryset(docs)


@router.post("/", response_model=Document_Pydantic, dependencies=[Depends(has_perm)])
async def create_document(form_data: DocumentIn_Pydantic, user: Member = Depends(get_current_user)):
    document_obj = await Document.create(**form_data.dict(exclude_unset=True), author=user)
    return await Document_Pydantic.from_tortoise_orm(document_obj)


@router.put("/{document_id}", response_model=Document_Pydantic, dependencies=[Depends(has_perm)])
async def update_user(document_id: int, document: DocumentIn_Pydantic, user: Member = Depends(get_current_user)):
    cat_id = document.dict(exclude_unset=True).get("category_id")
    if user.username != "admin":
        has_perm = await CategoryPermission.has_perm(user.id, cat_id, "update")
        if not has_perm:
            raise HTTPException(status_code=403, detail="You do not have access to this category")

    await Document.get(id=document_id).update(**document.dict(exclude_unset=True))

    return await Document_Pydantic.from_queryset_single(Document.get(id=document_id))


@router.delete("/{document_id}", dependencies=[Depends(has_perm)], response_model=str)
async def delete_user(document_id: int):
    await Document.filter(id=document_id).delete()
    return "Removed successfully"
