from fastapi import APIRouter, Depends

from src.models.permission import Permission
from src.models.schema import (
    CategoryPermission,
    CategoryPermission_Pydantic,
    DocumentPermission,
    DocumentPermission_Pydantic,
)
from src.utils.check_permission import has_perm

tags = ["Permission"]
router = APIRouter(tags=tags)


@router.post("/create", response_model=Permission, response_model_exclude_unset=True, dependencies=[Depends(has_perm)])
async def create_permission(form_data: Permission):
    category_id = form_data.dict(exclude_unset=True).get("category_id")
    # document_id = form_data.dict(exclude_unset=True).get("document_id")
    # member_id = form_data.dict(exclude_unset=True).get("member_id")
    if category_id:
        # temp_dict = {"member_id": member_id, "category_id": category_id}
        obj, _ = await CategoryPermission.update_or_create(**form_data.dict(exclude_unset=True))
        return await CategoryPermission_Pydantic.from_tortoise_orm(obj)
    else:
        # temp_dict = {"member_id": member_id, "document_id": document_id}
        obj, _ = await DocumentPermission.update_or_create(**form_data.dict(exclude_unset=True))
        return await DocumentPermission_Pydantic.from_tortoise_orm(obj)
